import 'dart:convert';

import 'package:darkbearschatapp/PreferencesManager.dart';
import 'package:darkbearschatapp/chat_model.dart';
import 'package:darkbearschatapp/common_methods.dart';
import 'package:darkbearschatapp/constants.dart';
import 'package:web_socket_channel/io.dart';

class ChatManager {
  final channel = IOWebSocketChannel.connect(Constants.WEB_SOCKET_ADDRESS);
    static ChatManager _instance = ChatManager._internal();
  
    static getInstance() => _instance;
    String userID;
  
    ChatManager._internal() {
      setUserId();
    }
  
    sendData(Chat chat) {
      channel.sink.add(json.encode(chat.toJson()));
    }
  
    setUserId() async {
      String user_id =
          PreferencesManager.getPref(PreferencesManager.USER_ID) ?? "";
      if (user_id.length == 0) {
        user_id = await CommonMethods().getId();
        PreferencesManager.savePref(PreferencesManager.USER_ID, user_id);
        this.userID = user_id;
        return this.userID;
      } else {
        this.userID = user_id;
        return user_id;
      }
    }
  
    getSocketChannelStream() {
      return channel.stream;
    }
  
    closeConnection() {
      channel.sink.close();
    }
  }
  

